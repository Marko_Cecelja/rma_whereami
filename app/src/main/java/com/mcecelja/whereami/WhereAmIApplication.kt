package com.mcecelja.whereami

import android.app.Application
import android.content.Context
import com.mcecelja.whereami.di.appModule
import com.mcecelja.whereami.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WhereAmIApplication : Application() {

    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        startKoin {
            androidContext(this@WhereAmIApplication)
            modules(appModule, viewModelModule)
        }
    }
}