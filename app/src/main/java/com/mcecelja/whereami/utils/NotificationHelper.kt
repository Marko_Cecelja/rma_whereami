package com.mcecelja.whereami.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationManagerCompat
import com.mcecelja.whereami.WhereAmIApplication

fun getChannelId(name: String): String = "${WhereAmIApplication.context.packageName}-$name"
const val PICTURE_TAKEN = "Picture_Channel"
@RequiresApi(api = Build.VERSION_CODES.O)
fun createNotificationChannel(name: String, description: String, importance: Int): NotificationChannel {
    val channel = NotificationChannel(getChannelId(name), name, importance)
    channel.description = description
    channel.setShowBadge(true)
    return channel
}
@RequiresApi(api = Build.VERSION_CODES.O)
fun createNotificationChannels() {
    val channels = mutableListOf<NotificationChannel>()
    channels.add(
        createNotificationChannel(
        PICTURE_TAKEN,
        "Picture taken!",
        NotificationManagerCompat.IMPORTANCE_DEFAULT
    )
    )
    val notificationManager = WhereAmIApplication.context.getSystemService(NotificationManager::class.java)
    notificationManager.createNotificationChannels(channels)
}