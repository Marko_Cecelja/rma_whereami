package com.mcecelja.whereami.utils

interface OnGpsListener {

    fun gpsStatus(isGPSEnable: Boolean)
}