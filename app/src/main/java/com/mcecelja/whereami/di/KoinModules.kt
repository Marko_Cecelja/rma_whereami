package com.mcecelja.whereami.di

import com.mcecelja.whereami.location.LocationLiveData
import com.mcecelja.whereami.sound.AudioPlayer
import com.mcecelja.whereami.sound.SoundPoolAudioPlayer
import com.mcecelja.whereami.ui.MapsViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    factory { LocationLiveData(androidContext()) }
    single<AudioPlayer> { SoundPoolAudioPlayer(androidContext()) }
}

val viewModelModule = module {
    viewModel { MapsViewModel(get(), get()) }
}