package com.mcecelja.whereami.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.mcecelja.whereami.location.LocationLiveData
import com.mcecelja.whereami.sound.AudioPlayer

class MapsViewModel(private val locationLiveData: LocationLiveData, private val audioPlayer: AudioPlayer) : ViewModel() {

    private val _marker: MutableLiveData<Marker> = MutableLiveData<Marker>()
    val marker: LiveData<Marker> = _marker

    fun getLocation() = locationLiveData

    fun setMarker(marker: Marker) {
        _marker.postValue(marker)
    }

    fun updatePosition(latLng: LatLng) {
        _marker.value?.position = latLng
    }

    fun playSound() = audioPlayer.playSound()
}