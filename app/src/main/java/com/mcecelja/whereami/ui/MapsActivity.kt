package com.mcecelja.whereami.ui

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.CAMERA
import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mcecelja.whereami.R
import com.mcecelja.whereami.databinding.ActivityMapsBinding
import com.mcecelja.whereami.enums.RequestEnum
import com.mcecelja.whereami.model.LocationModel
import com.mcecelja.whereami.utils.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.io.IOException
import java.util.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var binding: ActivityMapsBinding

    private lateinit var mMap: GoogleMap

    private val viewModel: MapsViewModel by viewModel()

    private var isGPSEnabled = false

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        GpsUtils(this).turnGPSOn(object : OnGpsListener {

            override fun gpsStatus(isGPSEnable: Boolean) {
                this@MapsActivity.isGPSEnabled = isGPSEnable
            }
        })
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) createNotificationChannels()
        binding.bPicture.setOnClickListener { takePicture() }
    }

    private fun takePicture() {
        try {
            if (isPermissionsGranted(CAMERA)) {

                val takePictureIntent = Intent(this, MapsActivity::class.java)

                val photoFile: File = createImageFile()

                val photoURI: Uri = FileProvider.getUriForFile(
                    this,
                    "",
                    photoFile
                )

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                resultLauncher.launch(takePictureIntent)
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        CAMERA
                    ),
                    RequestEnum.CAMERA_REQUEST.code
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val storageDir: File? = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "${viewModel.getLocation().value?.country}_${viewModel.getLocation().value?.city}_${viewModel.getLocation().value?.street}_${viewModel.getLocation().value?.homeNumber}",
            ".jpg",
            storageDir
        ).apply {
            sendPictureTakenNotification(absolutePath)
        }
    }

    private fun sendPictureTakenNotification(path: String) {
        val intent = Intent(this, MapsActivity::class.java)
        intent.action = Intent.ACTION_GET_CONTENT
        intent.setDataAndType(Uri.parse("file:$path"), "file/*")

        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            0
        )
        val notification = NotificationCompat.Builder(this, getChannelId(PICTURE_TAKEN))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Spremljena je nova slika!")
            .setContentText("file:$path")
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setProgress(0, 0, false)
            .build()
        NotificationManagerCompat.from(this)
            .notify(1001, notification)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapClickListener { addMarker(it) }

        val currentLocation = LatLng(0.0, 0.0)
        viewModel.setMarker(
            mMap.addMarker(
                MarkerOptions().position(currentLocation).title("Here I am!")
            )
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
        invokeLocationAction()
    }

    private fun addMarker(latLng: LatLng) {
        mMap.addMarker(
            MarkerOptions().position(latLng).title("Marker")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
        )

        viewModel.playSound()
    }

    private fun updateMap(locationModel: LocationModel) {
        val currentLocation = LatLng(locationModel.latitude, locationModel.longitude)
        viewModel.updatePosition(currentLocation)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16f))
    }

    private fun startLocationUpdate() {
        viewModel.getLocation().observe(
            this, { updateMap(it) }
        )
    }

    private fun invokeLocationAction() {
        when {

            isPermissionsGranted(ACCESS_FINE_LOCATION) -> startLocationUpdate()

            else -> ActivityCompat.requestPermissions(
                this,
                arrayOf(ACCESS_FINE_LOCATION),
                RequestEnum.LOCATION_REQUEST.code
            )
        }
    }

    private fun isPermissionsGranted(permission: String) =
        ActivityCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RequestEnum.LOCATION_REQUEST.code -> {
                invokeLocationAction()
            }
            RequestEnum.CAMERA_REQUEST.code -> {
                takePicture()
            }
        }
    }
}