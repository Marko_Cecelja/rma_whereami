package com.mcecelja.whereami.sound

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import com.mcecelja.whereami.R

class SoundPoolAudioPlayer(context: Context) : AudioPlayer {

    private val sound: Int

    private val soundPool: SoundPool

    init {
        soundPool =
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                val audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()

                SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .setMaxStreams(6)
                    .build()
            } else {
                SoundPool(6, AudioManager.STREAM_MUSIC, 1)
            }

        sound = soundPool.load(context, R.raw.blink, 1)
    }

    override fun playSound() {
        soundPool.play(sound ?: error("Unknown sound"), 1f, 1f, 1, 0, 1f)
    }
}