package com.mcecelja.whereami.sound

interface AudioPlayer {

    fun playSound()
}