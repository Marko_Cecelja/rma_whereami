package com.mcecelja.whereami.enums

enum class RequestEnum(val code: Int) {

    LOCATION_REQUEST(100),
    GPS_REQUEST(101),
    CAMERA_REQUEST(102)
}